import { ElDialog,ElInput,ElButton } from "element-plus";
import { createVNode,defineComponent,reactive,render } from "vue";
import ExPlain from "../packages/explain"


const PoPoverComponent = defineComponent({
    props:{
        option:{type:Object}
    },
    setup(props,ctx){
        const state = reactive({
            option:props.option,
            isShow:false
        })
        // expose()把函数里的方法暴露出去
        ctx.expose({
            // option是当前画布里组件元素的所有信息
            showDialog(option){
                // 更新新的画布里组件元素的所有信息
                state.option = option;
                state.isShow = true;
            }
        });
        // state.isShow = false则关闭弹窗
        const onCancel = () =>{
            state.isShow = false;
        }
        // 执行editor.jsx里的onConfirm方法 变为我们导入的json数据的样式画布布局
        const onConfirm = () =>{
            state.isShow = false;
            state.option.onConfirm && state.option.onConfirm(state.option.content)
        }
        return ()=>{
            // 
            return <ElDialog v-model={state.isShow} title={state.option.title}>
                {{
                    // default在vue3里表示插槽
                    // state.option.content为画布里组件元素的json信息
                    default:()=><ExPlain></ExPlain>,
                    // 根据state.option.footer是true还是false来确定是否显示取消、确定按钮
                    footer:()=>state.option.footer && <div>
                        <ElButton onClick={onCancel}>取消</ElButton>
                        <ElButton type="primary" onClick={onConfirm}>确定</ElButton>
                    </div>
                }}
            </ElDialog>
        }
    }
})
let vm;
export function $PoPover(option){

     //第一次点击导出时 表示没有创建vm弹窗元素 则创建
     if (!vm) {
        // createElement() 方法通过指定名称创建一个元素
        // 创建一个div元素
        let el = document.createElement('div');
        // createVNode()函数创建一个dom元素
        vm = createVNode(PoPoverComponent,{option});
        // appendChild()向节点的子节点列表的末尾添加新的子节点
        // 在body的末尾添加上面两个元素
        document.body.appendChild((render(vm,el),el))
    }
    
    let {showDialog} = vm.component.exposed
    //已经创建了vm弹窗元素 执行上面的showDialog函数  对option信息进行更新
    showDialog(option)
}