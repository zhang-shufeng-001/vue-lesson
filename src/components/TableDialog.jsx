import deepcopy from "deepcopy";
import { ElButton, ElDialog, ElInput, ElTable, ElTableColumn } from "element-plus";
import { reactive, render,defineComponent,createVNode } from "vue";

const TableComponent = defineComponent({
    props:{
        option:{type:Object}
    },
    setup(props,ctx){
        const state = reactive({
            option:props.option,
            isShow:false,
            editData:[]
        })
        let methods = {
            show(option){
                state.option = option;
                state.isShow = true;
                state.editData = deepcopy(option.data)
            }
        }
        const add = () =>{
            state.editData.push({})
        }
        const onCancel = ()=>{
            state.isShow = false;
        }
        const onConfrim = ()=>{
            state.option.onConfirm(state.editData);
            onCancel()
        }
        ctx.expose(methods);
        return ()=>{
            return <ElDialog v-model={state.isShow} title={state.option.config.label}>
                {{
                    default:()=>(
                        <div>
                            <div><ElButton onClick={add}>添加</ElButton><ElButton>重置</ElButton></div>
                            <ElTable data={state.editData}>
                                <ElTableColumn type="index"></ElTableColumn>
                                {state.option.config.table.options.map((item,index)=>{
                                    return <ElTableColumn label={item.label}>
                                        {{
                                            default:({row})=> <ElInput v-model={row[item.field]}>
                                            </ElInput>
                                        }}
                                    </ElTableColumn>
                                })}
                                <ElTableColumn label="操作">
                                    <ElButton type="danger">删除</ElButton>
                                </ElTableColumn>
                            </ElTable>
                        </div>
                    ),
                    footer:()=><>
                        <ElButton onClick={onCancel}>取消</ElButton>
                        <ElButton onClick={onConfrim}>确定</ElButton>
                    </>
                }}
            </ElDialog>
        }
    }
})

let vm;
export const $tableDialog = (option) =>{

    //
    if (!vm) {
        const el = document.createElement('div');
        // createVNode()函数创建一个dom元素
        vm = createVNode(TableComponent,{option});
        let r = render(vm,el)
        // appendChild()向节点的子节点列表的末尾添加新的子节点
        // 在body的末尾添加上面两个元素
        document.body.appendChild(el)
    }

    let { show } = vm.component.exposed;
    //已经创建了vm弹窗元素 执行上面的showDropdown函数  对option信息进行更新
    show(option)
}