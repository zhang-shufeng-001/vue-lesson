
import { computed, createVNode,defineComponent,inject,onBeforeUnmount,onMounted,provide,reactive,ref,render } from "vue";

export const DropdownItem = defineComponent({
    props:{
        label:String,
        icon:String,
    },
    setup(props){
        let {label} = props
        let hide = inject('hide')
        return ()=><div class="dropdown-item" onClick={hide}>
            <span>{label}</span>
        </div>
    }
})
const DropdownComponent = defineComponent({
    props:{
        option:{type:Object}
    },
    setup(props,ctx){
        const state = reactive({
            option:props.option,
            isShow:false,
            top:0,
            left:0,
        })
        // expose()把函数里的方法暴露出去
        ctx.expose({
            // option是当前画布里组件元素的所有信息
            showDropdown(option){
                // 更新新的画布里组件元素的所有信息
                state.option = option;
                state.isShow = true;
                let {top,left,height} = option.el.getBoundingClientRect();
                state.top = top + height;
                state.left = left;
            }
        });
        provide('hide',()=>state.isShow = false)

        const classes = computed(() =>[
            'dropdown',
            {
                'dropdown-isShow':state.isShow
            }
        ])
        const el = ref(null)
        const onMousedownDocument = (e) =>{
            if (!el.value.contains(e.target)) {
                state.isShow = false;
            }
        }

        onMounted(()=>{
           document.body.addEventListener('mousedown',onMousedownDocument,true)
        })

        const style = computed(() =>({
            top:state.top + 'px',
            left:state.left + 'px'
            }))
        
        onBeforeUnmount(()=>{
            document.body.removeEventListener('mousedown',onMousedownDocument)
        })
        return ()=>{
            return <div class={classes.value} style={style.value} ref={el}>
                {state.option.content()}
            </div>
        }
    }
})

let vm;
export function $dropdown(option){

    //第一次点击导出时 表示没有创建vm弹窗元素 则创建
    if (!vm) {
        // createElement() 方法通过指定名称创建一个元素
        // 创建一个div元素
        let el = document.createElement('div');
        // createVNode()函数创建一个dom元素
        vm = createVNode(DropdownComponent,{option});
        // appendChild()向节点的子节点列表的末尾添加新的子节点
        // 在body的末尾添加上面两个元素
        document.body.appendChild((render(vm,el),el))
    }
    
    let {showDropdown} = vm.component.exposed
    //已经创建了vm弹窗元素 执行上面的showDropdown函数  对option信息进行更新
    showDropdown(option)
}