import { reactive } from "vue";
import { events } from "./events";

// focusData为画布内点击选中有焦点的没有焦点的组件数据
export function useBlockDragger(focusData,lastSelectBlock,data){
    let dragState = {
        startX:0,
        startY:0,
        dragging:false  //默认不是正在拖拽
    }
    let markline = reactive({
        x:null,
        y:null,
    })
    // 鼠标一直着点击元素没有松开时
    const mousedown = (e) =>{
        // 解构出最后一个选中元素组件的宽高
        const {width:Bwidth,height:Bheight} = lastSelectBlock.value;
        // 定义一个对象获取当前点击元素的坐标
        dragState = {
            startX:e.clientX,
            startY:e.clientY,
            startLeft:lastSelectBlock.value.left,
            startTop:lastSelectBlock.value.top,
            dragging:false,
            // focusData.value.focus表示每个被选中的元素
            // 为每个选中的画布组件当前位置
            startPos:focusData.value.focus.map(({top,left})=>({top,left})),
            lines:(()=>{
                const {unfocused} = focusData.value;    //获取其他没选中的组件以它们位置做辅助线
                let lines = { x:[], y:[]};    // 计算横线位置用y来存放 x存的是纵向的
                [...unfocused,
                    {
                        top:0,
                        left:0,
                        width:data.value.container.width,
                        height:data.value.container.height,
                    }
                ].forEach((block)=>{
                    // top:离顶部的距离 left:离左边的距离 width:组件的宽度 height:组件的高度
                    const {top:Atop,left:Aleft,width:Awidth,height:Aheight} = block;
                    lines.y.push({showTop:Atop,top:Atop});  // 顶对顶，拖拽组件的顶部和对比组件的顶部平行
                    lines.y.push({showTop:Atop,top:Atop - Bheight});// 顶对底
                    lines.y.push({showTop:Atop + Aheight / 2, top:Atop + Aheight/2 - Bheight/2});//中对中
                    lines.y.push({showTop:Atop + Aheight, top:Atop + Aheight});//底对顶
                    lines.y.push({showTop:Atop + Aheight, top:Atop + Aheight - Bheight});//底对底

                    lines.x.push({showLeft:Aleft,left:Aleft});//左对左
                    lines.x.push({showLeft:Aleft + Awidth,left:Aleft + Awidth});//右对左
                    lines.x.push({showLeft:Aleft + Awidth/2,left:Aleft + Awidth/2 - Bwidth/2});//中对中
                    lines.x.push({showLeft:Aleft + Awidth,left:Aleft + Awidth - Bwidth});//右对右
                    lines.x.push({showLeft:Aleft,left:Aleft - Bwidth});//左对右
                });
                return lines
            })()
        }
        document.addEventListener('mousemove',mousemove)
        document.addEventListener('mouseup',mouseup)
    }
    // 鼠标开始移动时
    const mousemove = (e) =>{
        // 结构出当前在移动的距离
        let {clientX:moveX,clientY:moveY} = e;
        if (!dragState.dragging) {
            // 那就是在拖拽
            dragState.dragging = true;
            events.emit('start')    //那就保存当前的画布内容
        }
        // 计算当前元素最新的left和top去线里面找，找到显示线
        // 鼠标移动后-鼠标移动前 + left就好了
        let left = moveX - dragState.startX + dragState.startLeft;
        let top = moveY - dragState.startY + dragState.startTop;

        // 先计算横线 距离参照物元素还有5像素的时候 就显示这根线
        let y = null;
        let x = null;
        for (let i = 0; i < dragState.lines.y.length; i++) {
            const { top:t,showTop:s } = dragState.lines.y[i];//获取每一根线
            if(Math.abs(t - top) < 5){  //如果小于5说明接近了
                y = s;  //线要实现的位置
                moveY  = dragState.startY - dragState.startTop + t;     //容器距离顶部的距离+目标的高度

                break;
            }
        }
        for (let i = 0; i < dragState.lines.x.length; i++) {
            const { left:l,showLeft:s } = dragState.lines.x[i];
            if(Math.abs(l - left) < 5){  //如果小于5说明接近了
                x = s;  //线要实现的位置
                moveX  = dragState.startX - dragState.startLeft + l;     //容器距离顶部的距离+目标的高度
                break;
            }
        }
        markline.x = x;     //markline是一个响应式数据，x，y更新了会导致视图更新
        markline.y = y;

        // 获取当前移动组件离原来位置的x y距离
        let durX = moveX - dragState.startX;
        let durY = moveY - dragState.startY;
        // 让每个选中的组件的原来位置坐标加上上面计算出的距离则就是松手后的新x y坐标
        focusData.value.focus.forEach((block,idx)=>{
            block.top = dragState.startPos[idx].top + durY;
            block.left = dragState.startPos[idx].left + durX;
        })
    }
    // 鼠标点击元素松开后触发
    const mouseup = (e) =>{
        document.removeEventListener('mousemove',mousemove)
        document.removeEventListener('mouseup',mouseup)
        markline.x = null;     //markline是一个响应式数据，x，y更新了会导致视图更新
        markline.y = null;
        if (dragState.dragging) {
            events.emit('end')
        }
    }  
    return {mousedown,markline}
}