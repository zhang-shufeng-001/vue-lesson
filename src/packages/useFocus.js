import {computed,ref} from 'vue'
// data为新的有松手地址数据     callback为回调函数
export function useFocus(data,previewRef,callback){
    // 表示没有选中任何一个组件
    const selectIndex = ref(-1);
    // 最后选择的哪一个组件
    const lastSelectBlock = computed(()=>data.value.blocks[selectIndex.value])

    // 获取到哪些元素被选中、哪些没被选中 后面好做移动操作
    const focusData = computed(()=>{
        let focus = [];
        let unfocused = [];
        data.value.blocks.forEach(block => (block.focus ? focus : unfocused).push(block))
        return {focus,unfocused}
    }) 

    //点击另外一个元素时失去上一个元素焦点
    const clearBlockFocus = () => {
        data.value.blocks.forEach(block=> block.focus = false)
    }

    // 点击容器失去焦点
    const containerMousedown = () =>{
        if (previewRef.value) return
        clearBlockFocus();
        selectIndex.value = -1;
    }
    // 实现获取焦点
    const blockMousedown = (e,block,index) => {
        // 改为
        if (previewRef.value) return
        // 阻止浏览器的默认行为
        e.preventDefault();
        // 阻止事件冒泡
        e.stopPropagation();
        // 按住shift键点击时能获取多个焦点
        if (e.shiftKey) {
            // 在block上定义一个属性focus true为获取焦点，false为失去焦点
                if (focusData.value.focus.length <= 1) {
                    // 在摁住shift 只有一个组件元素被选中时
                    block.focus = true;     
                }else{
                    block.focus = !block.focus;
                }
            } else {
                if (!block.focus) {
                    clearBlockFocus(); //点击另外一个元素时失去上一个元素焦点
                    block.focus = true
                }
            }
        selectIndex.value = index;
        callback(e)
    }
    return{blockMousedown,focusData,containerMousedown,lastSelectBlock,clearBlockFocus}
}