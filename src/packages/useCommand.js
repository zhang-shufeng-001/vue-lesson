import deepcopy from "deepcopy";
import { events } from "./events";
import {onUnmounted} from "vue"

// 这里面的data是指画布里的初始数据
export function useCommand(data,focusData){
    const state = {     //前进后退需要指针
        current: -1,    //前进后退的索引值
        queue: [],      //存放所有的操作命令
        commands: {},   //制作命令和执行功能一个映射表  undo : ()=>{}   redo:()=>{}
        commandArray: [],   //存放所有的命令
        destroyArray:[]     //销毁列表
    }
    
    const registry = (command) => {
        // 存放所有的指令
        state.commandArray.push(command);
        // 映射表根据name去对应相同的对象   根据命令的name来对应执行函数
        state.commands[command.name] = (...args) =>{
            // 结构execute里的redo,undo两个函数
            const {redo,undo} = command.execute(...args);
            // 执行一次redo函数
            redo();
            // 判断进入的指令是否有pushQueue这个元素
            if (!command.pushQueue) {
                return          //没有就执行到这里 不执行下去
            }
            let {queue,current} = state
            // queue存放所有的操作命令
            // 如果queue里有数据
            if (queue.length > 0) {
                // 哪就切割从第一次执行操作命令到最后一步执行操作命令
                // 因为有可能在多次执行拖拽完成松手操作里，使用了撤销命令然后再次进行拖拽组件到画布里去
                // 比如组件1 =》组件2 =》组件3 =》撤销 =》组件4 
                // 哪就会把执行了撤销操作的组件删除     组件1 =》组件2  =》组件4 
                queue = queue.slice(0,current + 1);
                
                state.queue = queue;
            }
            // 这里的redo代表:松手后的画布内容  undo代表:拖拽之前的画布内容
            // 每执行一次拖拽完成松手后则添加一次操作数据
            queue.push({redo,undo});
            // state.current加1    每执行一次拖拽完成松手后则让数量加1
            state.current = current + 1;
        }
    }

    // 注册我们需要的命令
    // 重做功能
    registry({
        name:'redo',
        keyboard:'ctrl+y',
        execute(){
            return{
                // 使用了撤销功能   才能使用重做功能
                redo(){
                    // 因为使用了撤销state.current会减1 所以state.current+1为刚刚撤销的索引
                    let item = state.queue[state.current+1];
                    if (item) {
                        // redo为拖拽松手之后的画布的内容
                        item.redo && item.redo();
                        // 因为撤销操作会减减 所以在把它调到原来的索引位置上去
                        state.current++;
                    }
                }
            }
        }
    })
    // 撤销功能
    registry({
        name:'undo',
        keyboard:'ctrl+z',
        execute(){
            return{
                redo(){
                    // 如果state.current == -1代表没有进行操作 则不执行下去
                    if (state.current == -1) return;
                    // 如果state.current不等于-1  则此时的state.queue[state.current]为上一步操作
                    let item = state.queue[state.current];
                    if (item) {
                        // 那就执行它的undo方法  undo为拖拽之前就记录画布内的内容
                        item.undo && item.undo();
                        // 在把索引调为上一步
                        state.current--;
                    }
                }
            }
        }
    });
    //如果希望将操作放到队列中可以增加一个属性 标识等会操作要放到队列中
    registry({      
        name:'drag',
        //如果希望将操作放到队列中可以增加一个属性 标识等会操作要放到队列中
        pushQueue:true,
        init(){     //初始化操作  默认就会执行
            this.before = null
            // 在拖拽开始就记录画布内的内容 保存状态 深拷贝data数据   data为新的有松手地址数据
            const start = () =>{
                this.before = deepcopy(data.value.blocks)
            } 
            // 拖拽松手之后 则执行自身execute函数
            const end = () => state.commands.drag()
            // events.on注册并监听自定义事件   在useMenuDragger.js里拖拽开始、松手会使用emit去触发这两个事件
            events.on('start',start)
            events.on('end',end)
            return () =>{
                // events.on取消监听自定义事件
                events.off('start',start)
                events.off('end',end)
            }
        },
        execute(){
            //  this.before为拖拽之前就记录画布内的内容
            let before = this.before;
            // 拖拽松手之后的画布的内容
            let after = data.value.blocks;  
            return{
                redo(){ //默认拖拽一松手 就会执行
                    data.value = {...data.value,blocks:after}
                },
                undo(){ //  点击撤销 则blocks改为拖拽之前就记录画布内的内容
                    data.value = {...data.value,blocks:before}
                }
            }
        }
    })
     // 带有历史记录的常用模式
     registry({
        name:'updateContainer',
        pushQueue:true,
        execute(newValue){
            let state = {   
                // 当前的画布内组件所有信息  
               before:data.value,
               // 新的画布内组件所有信息  
               after:newValue
            }
            return{
                redo:()=>{
                    data.value = state.after
                },
                undo:()=>{
                    data.value = state.before
                }
            }
        }
    });
    // 置顶操作
    registry({
        name:'placeTop',
        pushQueue:true,
        execute(){
            // 置顶之前的画布内容
            let before = deepcopy(data.value.blocks);
            // 置顶之后的画布内容
            let after = (()=>{
                // focus：代表被选中的元素  unfocused：代表没选中的
                let {focus,unfocused} = focusData.value;
                // 找出所有没被选中元素的最大的zIndex
                let maxZIndex = unfocused.reduce((prev,block)=>{
                    // Math.max找最大值
                    return Math.max(prev,block.zIndex);
                    // -Infinity表示 负无穷
                },-Infinity);
                // 把选中元素的zIndex在最大的没有被选中元素上的zIndex加1那就是最大的
                focus.forEach(block => block.zIndex = maxZIndex + 1);
                return data.value.blocks
            })()

            return{
                // 点击按钮后则执行这个函数
                redo:()=>{
                    data.value = {...data.value,blocks:after}
                },
                // 点击撤销按钮才会执行这个函数
                undo:()=>{
                    data.value = {...data.value,blocks:before}
                }
            }
        }
    });
     // 置底操作
     registry({
        name:'placeBottom',
        pushQueue:true,
        execute(){
             // 置底之前的画布内容
            let before = deepcopy(data.value.blocks);
            // 置底之后的画布内容
            // 箭头后面跟的是JSX表达式时, 只需要使用();
            let after = (()=>{
                let {focus,unfocused} = focusData.value;
                // 找出所有没被选中元素的最小的zIndex
                let minZIndex = unfocused.reduce((prev,block)=>{
                    return Math.min(prev,block.zIndex);
                },Infinity) - 1;
                // 如果minZIndex为负数 说明最小值为0 此时minZIndex为-1
                if (minZIndex < 0) {
                    // Math.abs表示绝对值
                    const dur = Math.abs(minZIndex);
                    // 把minZIndex设为0
                    minZIndex = 0;
                    // 让所有没选中的的zIndex+1
                    unfocused.forEach(block => block.zIndex += dur)
                }
                // 如果为minZIndex为0,说明没选中最小为1  如果为负数,还是把minZIndex设为0,但是会走上面的if判断
                // 让所有没选中的加1  所以block.zIndex必定是最小的
                // 让点击的元素等于最小的没选中的zIndex  
                focus.forEach(block => block.zIndex = minZIndex);
                return data.value.blocks
            })()

            return{
                redo:()=>{
                    data.value = {...data.value,blocks:after}
                },
                undo:()=>{
                    data.value = {...data.value,blocks:before}
                }
            }
        }
    });
     // 删除操作
     registry({
        name:'delete',
        pushQueue:true,
        execute(){
            let state = {   
                // 当前的画布内组件所有信息  
               before:deepcopy(data.value.blocks),
               // 表示没有被选中的画布元素
               after:focusData.value.unfocused
            }
            return{
                // 把当前画布内容改为没有被选中的画布元素,哪就相当于删除了选中的元素
                redo:()=>{
                    data.value = {...data.value,blocks:state.after}
                },
                undo:()=>{
                    data.value = {...data.value,blocks:state.before}
                }
            }
        }
    });
     // 右键的导入功能
     registry({
        name:'updateBlock',
        pushQueue:true,
        execute(newBlock,oldBlock){
            let state = {   
                // 当前的画布内组件所有信息  
               before:data.value.blocks,
               // 新的画布内组件所有信息  
               after:(()=>{
                let blocks = [...data.value.blocks];
                const index = data.value.blocks.indexOf(oldBlock);
                if(index > -1){
                    blocks.splice(index,1,newBlock)
                }
                return blocks;
               })()
            }
            return{
                redo:()=>{
                    data.value = {...data.value,blocks:state.after}
                },
                undo:()=>{
                    data.value = {...data.value,blocks:state.before}
                }
            }
        }
    });

    const keyboardEvent = (()=>{
        const keyCodes = {
           90:'z',
           89:'y'
        }
        // 使用键盘事件快捷键
        const onKeydown = (e) =>{
            const {ctrlKey,keyCode} = e;
            let keyString = [];
            if (ctrlKey) keyString.push('ctrl');
            keyString.push(keyCodes[keyCode]);
            keyString = keyString.join('+');

            state.commandArray.forEach(({ keyboard,name }) => {
                if(!keyboard) return;
                if (keyboard === keyString) {
                    state.commands[name]();
                    e.preventDefault();
                }
            })
        }

        const init = () =>{
            window.addEventListener('keydown',onKeydown)
            return ()=>{
                window.removeEventListener('keydown',onKeydown)
            }
        }
        return init
    })()
    ;(()=>{
        state.destroyArray.push(keyboardEvent())
        // 如果在commandArray里有init函数   则把它添加进destroyArray销毁数组里去
        state.commandArray.forEach(command=>command.init && state.destroyArray.push(command.init()))
    })();
    
    onUnmounted(()=>{   //清理绑定的事件
        // 执行destroyArray销毁数组里的函数
        state.destroyArray.forEach(fn => fn && fn());
    })
    return state;
}