import { events } from "./events";

// containerRef为整个画布元素   data为data.json里的数据
export function useMenuDragger(containerRef,data){
    // 定义一个元素获取被拖拽的组件对象，默认为空
    let currentComponent = null;
    // 拖拽至容器元素中显示一个拖动的图标
    const dragenter = (e) => {
        e.dataTransfer.dropEffect = 'move'
    }
    // 取消容器内的默认行为
    const dragover = (e) => {
        // e.preventDefault()   取消一个目标元素的默认行为
        e.preventDefault();
    }
    // 拖拽对象不在其容器范围内时 添加一个禁止移动的标识
    const dragleave = (e) => {
        e.dataTransfer.dropEffect = 'none'
    }
    // 物料区组件元素被拖动放手后执行
    const drop = (e) => {
        // 拿到画布里的组件组件数据
        let blocks = data.value.blocks;
        // 把画布里的组件对象进行重写、添加元素
        data.value = {...data.value,blocks:[
            ...blocks,
            // 在松手时获取到组件拖拽到什么地方的松手位置等信息
            {
                top:e.offsetY,
                left:e.offsetX,
                zIndex:1,
                // 获取被拖拽组件的key值
                key:currentComponent.key,
                // 在松手时元素居中
                alignCenter:true,
                props:{},
                model:{}
            }
        ]}
        // 把当前被拖拽的组件对象清空
        currentComponent = null;
    }

    // 物料区组件元素被拖动时执行
    const dragstart = (e,component) => {
        /*  ondragenter - 当被鼠标拖动的对象进入其容器范围内时触发此事件
            ondragover - 当某被拖动的对象在另一对象容器范围内拖动时触发此事件
            ondragleave - 当被鼠标拖动的对象离开其容器范围内时触发此事件
            ondrop - 在一个拖动过程中，释放鼠标键时触发此事件
        */
        // containerRef.value.addEventListener表示在画布区域里才进行事件委托，触发事件
        containerRef.value.addEventListener('dragenter',dragenter);
        containerRef.value.addEventListener('dragover',dragover);
        containerRef.value.addEventListener('dragleave',dragleave);
        containerRef.value.addEventListener('drop',drop);
        // 把被拖拽的组件数据赋值给currentComponent
        currentComponent = component;
        events.emit('start');//触发useCommand.js里的drag.init函数里的start自定义函数
    }
    // 关闭事件委托 组件拖拽松手后
    const dragend = (e) => {
        containerRef.value.removeEventListener('dragenter',dragenter);
        containerRef.value.removeEventListener('dragover',dragover);
        containerRef.value.removeEventListener('dragleave',dragleave);
        containerRef.value.removeEventListener('drop',drop);
        events.emit('end');//触发useCommand.js里的drag.init函数里的end自定义函数
    }
    return {
        dragstart,
        dragend
    }
}