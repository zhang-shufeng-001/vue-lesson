import { formEmits } from "element-plus";
import { computed, defineComponent, inject,onMounted,ref} from "vue";
import BlockResize from "./block.resize";

export default defineComponent({
    
    setup(){
        return ()=>{
          return  <div style={"color:blue"}>
                <div class="author">作者：小张同学</div>
                <br/>
                <div class="explain">
                    <span>
                        1.左侧为物料区组件区域:可将组件拖拽至画布区域进行编辑。<br/>
                        2.上方为菜单区域:具有相对应的功能按钮。<br/>
                        3.中间为画布区域:可存放从物料区拖拽过来的组件，可以根据辅助线来调整对应位置，例如:居中、平行等。<br/>
                        4.右侧为属性区域:可对画布和画布里的组件进行属性配置，例如:调整画布大小，修改组件内容、颜色。<br/>
                        5.按住shift键在点击可进行多选，点击画布空白区域可取消所有选中组件。<br/>
                    </span>
                    <span>6.点击撤销按钮或者按住<span style={"color:red"}>ctrl+z</span>返回你的上一步操作。</span>
                    <span>7.点击重做按钮或者按住<span style={"color:red"}>ctrl+y</span>返回撤销的上一步操作。</span>
                    <span>8.导出按钮可根据你当前画布布局生成json数据，可以对json里的数据进行修改。</span>
                    <span>9.导入按钮可根据你写入的json数据在画布里生成对应的组件。</span>
                    <span>10.当两个组件进行重叠时，点击置顶按钮会让当前选中组件在最上方。</span>
                    <span>11.当两个组件进行重叠时，点击置底按钮会让当前选中组件在最下方。</span>
                    <span>12.点击删除按钮会让选中的组件在画布里删除。</span>
                    <span>13.点击预览按钮不能对画布里的组件进行拖拽移动，点击编辑按钮则切回编辑模式。</span>
                    <span>14.点击关闭按钮会进入预览模式，点击下方继续编辑按钮则切回功能模式。</span>
                </div>
            </div>
        } 
    }
    
})