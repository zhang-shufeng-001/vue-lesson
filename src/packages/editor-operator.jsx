
import deepcopy from "deepcopy";
import { ElButton, ElColorPicker, ElForm,ElFormItem, ElInput, ElInputNumber, ElSelect,ElOption } from "element-plus";
import { defineComponent, inject, watch,reactive, onUpdated } from "vue";
import TableEditor from "./table-editor";

export default defineComponent({
    props:{
        // 最后选择的一个元素
        block:{type:Object},
        // json数据
        data:{type:Object},
        updateContainer:{type:Function},
        updateBlock:{type:Function}
    },
    setup(props,ctx){
        const config = inject('config');
        // 存放的信息
        const state = reactive({
            editData:{}
        })
        // 
        const reset = () =>{
            // 如果没有最后选择的一个元素 editData为json的初始画布容器宽高信息
            if (!props.block) {
                state.editData = deepcopy(props.data.container)
            }else{
                // 否则就显示 editData为最后选择的一个元素的信息
                state.editData = deepcopy(props.block);
            }
        }

        const apply = () =>{
            // 如果没有最后选择的一个元素 editData为最后选择的一个元素的信息 
            if (!props.block) {
                props.updateContainer({...props.data,container:state.editData});
            }else{
                props.updateBlock(state.editData,props.block);
            }
        }
        // 监控block最后选择的一个元素有没有发生变化  初始化执行一次
        watch(()=>props.block,reset,{immediate:true})
        return ()=>{
            // 定义这个变量 存放模板
            let content = []
            // 没有最后选择的一个元素 那就显示这个模板
            if (!props.block) {
                content.push(<>
                    <ElFormItem label="容器宽度">
                        <ElInputNumber v-model={state.editData.width}></ElInputNumber>
                    </ElFormItem>
                    <ElFormItem label="容器高度">
                        <ElInputNumber v-model={state.editData.height}></ElInputNumber>
                    </ElFormItem>
                </>)
            }else{
            // 否则就显示这个最后选择的一个元素属性模板  根据最后选择的一个元素的key来获取对应数据
               let component = config.componentMap[props.block.key];
                // 如果有props属性
               if (component && component.props) {
                // propName为component.props里的所有属性名  propConfig为component.props里的属性值
                
                content.push(Object.entries(component.props).map(([propName,propConfig])=>{
                    console.log(propConfig.type);
                    return <ElFormItem label={propConfig.label}>
                        {{
                            input:()=><ElInput v-model={state.editData.props[propName]}></ElInput>,
                            color:()=><ElColorPicker v-model={state.editData.props[propName]}></ElColorPicker>,
                            select:()=><ElSelect v-model={state.editData.props[propName]}>
                                {propConfig.options.map(opt=>{
                                    return <ElOption label={opt.label} value={opt.value}></ElOption>
                                })}
                            </ElSelect>,
                            table:()=> <TableEditor 
                            propConfig={propConfig} 
                            v-model={state.editData.props[propName]}></TableEditor>
                            // 映射  根据映射type对应state.editData.props[propName]里的数据
                        }[propConfig.type]()}
                    </ElFormItem>
                }))    
               }
            //    如果带有input则显示
               if(component && component.model){
                content.push(Object.entries(component.model).map(([modelName,label])=>{
                    return <ElFormItem label={label}>
                        <ElInput v-model={state.editData.model[modelName]}></ElInput>
                    </ElFormItem>
                }))
            }
        }


        return <ElForm labelPosition="top" style="padding:30px">
            {content}
            <ElFormItem>
                <ElButton type="primary" onClick={()=>apply()}>应用</ElButton>
                <ElButton onClick={reset}>重置</ElButton>
            </ElFormItem>

        </ElForm>
        }
    }
})
