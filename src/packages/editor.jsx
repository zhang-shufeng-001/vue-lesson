import {computed, defineComponent,inject,ref } from "vue"
import "./editor.scss"
import EditorBlocks from "./editor-blocks"
import deepcopy from "deepcopy"
import {useMenuDragger} from "./useMenuDragger.js"
import {useFocus} from "./useFocus.js"
import {useBlockDragger} from "./useBlockDragger.js"
import {useCommand} from "./useCommand.js"
import { $dialog } from "../components/Dialog"
import { $dropdown, DropdownItem } from "../components/Dropdown"
import { ElButton } from "element-plus"
import EditorOperator from "./editor-operator"
import { $PoPover } from "../components/PoPover"


export default defineComponent({
    props:{
        // 拿到初始画布数据 并定义类型为object
        modelValue:{type:Object},
        // 账号密码数据
        formData:{type:Object}
    },
    // 表示在当前组件自定义一个update:modelValue事件 会在组件标签解析一个:modelValue 和一个@update:modelValue
    emits:['update:modelValue'],
    
    setup(props,ctx){
        // 预览的时候 内容不能操作了  可以点击输入内容 方便看效果
        const previewRef = ref(false);
        const editorRef = ref(true);

        
        const data = computed({
            // 读取data则获取到props里的modelValue初始画布数据
            get(){
                return props.modelValue
            },
            // 拖拽组件对象拖进容器内一松手就更新物料区对象，并触发自定义事件，把原来的modelValue数据改成新传进
            // 来的数据
            set(newValue){
                // 触发自定义事件
                ctx.emit('update:modelValue',deepcopy(newValue))
            }
        })


        // container为画布容器的宽高属性  获取到容器的宽高
        const containerStyles = computed(()=>({
            width:data.value.container.width + "px",
            height:data.value.container.height + "px"
        }))

        
        //将所有物料区组件数据传入
      //里面有config.componentList：所有物料区组件数据  config.componentMap：带有key值的所有物料区组件数据
        const config = inject('config');

        // 定义一个元素获取画布元素
        const containerRef = ref(null)

        // 1.引入useMenuDragger.js文件  实现物料区的拖拽功能   
        const {dragstart,dragend} = useMenuDragger(containerRef,data)

        // 第二个箭头函数是回调函数
        let {blockMousedown,focusData,containerMousedown,lastSelectBlock,clearBlockFocus} = 
        useFocus(data,previewRef,(e)=>{
            mousedown(e)
        });
        // 2.实现画布内组件的拖拽功能  点击直接进行拖拽
        let {mousedown,markline} = useBlockDragger(focusData,lastSelectBlock,data);
        
        // 3.实现拖拽多个元素的功能
        
        // 撤销 重做功能
        const {commands} = useCommand(data,focusData);
        const buttons = [
            {label:'撤销',icon:'icon-back',handler:()=>commands.undo()},
            {label:'重做',icon:'icon-forward',handler:()=>commands.redo()},
            {label:'导出',icon:'icon-export',handler:()=>{
                $dialog({
                    title:'导出Json使用',
                    content:JSON.stringify(data.value),
                })
            }},
            {label:'导入',icon:'icon-import',handler:()=>{
                $dialog({
                    title:'导入Json使用',
                    content:'',
                    footer:true,
                    onConfirm(text){
                        // 去执行useCommand.js里的updateContainer方法 变为我们导入的json数据的样式画布布局
                        commands.updateContainer(JSON.parse(text));
                    }
                })
            }},
            {label:'说明',icon:'icon-place-top',handler:()=>{
                $PoPover({
                    title:'低代码平台使用说明',
                    footer:true,
                })
            }},
            {label:'置顶',icon:'icon-place-top',handler:()=>commands.placeTop()},
            {label:'置底',icon:'icon-place-bottom',handler:()=>commands.placeBottom()},
            {label:'删除',icon:'icon-delete',handler:()=>commands.delete()},
            {label:()=>previewRef.value ? '编辑':'预览',icon:()=>previewRef.value ?
            'icon-edit':'icon-browse',handler:()=>{
                previewRef.value = !previewRef.value;
                clearBlockFocus();
            }},
            {label:'关闭',icon:'icon-close',handler:()=>{
                editorRef.value = false;
                clearBlockFocus();
            }},
        ]

        const onContextMenuBlock = (e,block)=>{
            e.preventDefault();
            $dropdown({
                el:e.target,
                content:()=>{
                    return <>
                    <DropdownItem label="删除" icon="icon-delete" onClick={()=>commands.delete()}>
                    </DropdownItem>
                    <DropdownItem label="置顶" icon="icon-delete" onClick={()=>commands.placeTop()}>
                    </DropdownItem>
                    <DropdownItem label="置底" icon="icon-delete" onClick={()=>commands.placeBottom()}>
                    </DropdownItem>
                    <DropdownItem label="查看" icon="icon-delete" onClick={()=>{
                        $dialog({
                            title:'查看节点数据',
                            content:JSON.stringify(block)
                        })
                    }}>
                    </DropdownItem>
                    <DropdownItem label="导入" icon="icon-delete" onClick={()=>{
                        $dialog({
                            title:'导入节点数据',
                            content:'',
                            footer:true,
                            onConfirm(text){
                                text = JSON.parse(text);
                                commands.updateBlock(text,block)
                            }
                        })
                    }}>
                    </DropdownItem>
                    </>
                }
            })
        }

        // 看return里写的是什么是 dom元素就返回dom
        // 这里有个一元三次显示符操作
        return ()=> !editorRef.value ? <>
            <div className="editor-container-canvas__content" style={containerStyles.value} style="margin:0">
            {
                (data.value.blocks.map((block)=>( 
                <EditorBlocks 
                    class={'editor-block-preview'}
                    block={block}
                    formData={props.formData}
                ></EditorBlocks>
                )))
            }
            </div> 
            <div>
                <ElButton type="primary" onClick={()=>editorRef.value=true}>继续编辑</ElButton>
                {JSON.stringify(props.formData)}
            </div> 
        </> : <div class="editor">
            {/* 左边物料区 */}
            <div className="editor-left">
                {/* componentList为editor.config.jsx里的组件数据 */}
                {config.componentList.map(component=>(
                    // onDragstart - 用户开始拖动元素时触发的事件 把当前拖拽的组件数据传过去
                    <div className="editor-left-item"
                         draggable
                         onDragstart={e => dragstart(e,component)}
                         onDragend = {dragend}
                    >
                        <span>{component.label}</span>
                        <div>{component.preview()}</div>
                    </div>
                ))}
            </div>

            {/* 上方菜单区 */}
            <div className="editor-top">
                {buttons.map((btn,index)=>{
                    const icon = typeof btn.icon == 'function' ? btn.icon() : btn.icon
                    const label = typeof btn.label == 'function' ? btn.label() : btn.label
                    return <div class="editor-top-button" onClick={btn.handler}>
                            <button>{label}</button>
                    </div>
                })}
            </div>

            {/* 右边属性控制栏目 */}
            <div className="editor-right">
                {/* lastSelectBlock为最后选中的元素  data为当前所有的数据*/}
                <EditorOperator 
                block={lastSelectBlock.value} 
                data={data.value}
                updateContainer={commands.updateContainer}
                updateBlock={commands.updateBlock}>
                </EditorOperator>
            </div>

            {/* 中间画布区 */}
            <div className="editor-container">
                {/* 负责产生滚动条 */}
                <div className="editor-container-canvas">
                    {/* 负责产生内容区域 */}
                    <div className="editor-container-canvas__content" 
                        style={containerStyles.value}
                        ref={containerRef}
                        onMousedown={containerMousedown}>
                        {
                            // blocks为画布里面的组件数据 根据blocks里的数据来遍历出相对应的组件
                            (data.value.blocks.map((block,index)=>( 
                                // EditorBlocks为引入渲染画布内组件的jsx文件
                            <EditorBlocks 
                                class={block.focus ? 'editor-block-focus' : ''}
                                class={previewRef.value ? 'editor-block-preview' : ''}
                                block={block}
                                // block为当前点击的组件
                                onMousedown={(e) => blockMousedown(e,block,index)}
                                // onContextmenu用户鼠标右键事件
                                onContextmenu={(e) => onContextMenuBlock(e,block)}
                                formData={props.formData}
                            ></EditorBlocks>
                            )))
                        }

                    {markline.x !== null && <div class="line-x" style={{left:markline.x+'px'}}></div>}
                    {markline.y !== null && <div class="line-y" style={{top:markline.y+'px'}}></div>}
                    </div>
                </div>
            </div>
        </div>
    }
})