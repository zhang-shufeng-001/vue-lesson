import { formEmits } from "element-plus";
import { computed, defineComponent, inject,onMounted,ref} from "vue";
import BlockResize from "./block.resize";

export default defineComponent({
    props:{
        // block为画布内组件元素信息数据
        block:{type:Object},
        formData:{type:Object}
    },
    setup(props){
        const blockStyle = computed(()=>({
            top:`${props.block.top}px`,
            left:`${props.block.left}px`,
            zIndex:`${props.block.zIndex}`
        }));
        //将所有物料区组件数据引入
        //里面有config.componentList：所有物料区组件数据  config.componentMap：带有key值的所有物料区组件数据
        const config = inject('config');

        // 拖拽松手后blockRef为拖拽过来的组件元素
        const blockRef = ref(null)
        onMounted(()=>{
            let {offsetWidth,offsetHeight} = blockRef.value
            //只有目前是拖拽松手的组件元素才执行的，其他默认在画布上的内容不用
            if(props.block.alignCenter){    
                props.block.left = props.block.left - offsetWidth / 2;
                props.block.top = props.block.top - offsetHeight / 2;
                props.block.alignCenter = false;
            }
            props.block.width = offsetWidth;
            props.block.height = offsetHeight
        })

        return ()=>{
            // block画布内组件元素信息数据  data.json
            // 根据block的key来获取相对应的物料区组件数据  editor.config.jsx
            const component = config.componentMap[props.block.key];
            // 获取渲染出的组件,使用component里面的rander函数
            const RenderComponent = component.rander({
                size:props.block.hasResize ? {width:props.block.width,height:props.block.height} : {},
                props:props.block.props,
                model:Object.keys(component.model || {}).reduce((prev,modelName)=>{
                    let propName = props.block.model[modelName]
                    prev[modelName] = {
                        modelValue:props.formData[propName],
                        "onUpdate:modelValue": v=> props.formData[propName] = v
                    }
                    return prev;
                },{})
            });
            const {width,height} = component.resize || {}
            return <div class="editor-block" style={blockStyle.value} ref={blockRef}>
                    {RenderComponent}

                    {props.block.focus && (width || height) && <BlockResize
                        block={props.block}
                        component={component}
                        ></BlockResize>}
            </div>
        }
    }
})