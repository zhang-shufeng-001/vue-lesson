import { createApp } from 'vue'
import ElementPlus from 'element-plus'
// 只是使用element-plus的css样式
import 'element-plus/dist/index.css'
import App from './App.vue';

const app =  createApp(App)
// 屏蔽警告信息
app.config.warnHandler = () => null;

app.mount("#app");

// createApp(App).mount('#app')

// app.use(ElementPlus)

